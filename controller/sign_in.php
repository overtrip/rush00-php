<?php
function check_existing_login($login)
{
	if (!file_exists("/db/accounts.csv"))
		return true;
	$handle = fopen("/db/accounts.csv", "r");
	if (!$handle)
	{
		$_SESSION['ret'] =  "Erreur lors de l'acces a la base de donnees.\n";
		return false;
	}
	while ($array = fgetcsv($handle, 0, ","))
	{
		if ($array[0] === $login)
		{
			$_SESSION['ret'] =  "Erreur: login deja utilise.\n";
			return false;
		}
	}
	return true;
}

function stock_sign_in($login, $passwd)
{
	if ($login && $passwd)
	{
		if (!check_existing_login($login))
			return false;
		$handle = fopen("accounts.csv", 'a');
		if (!$handle)
		{
			$_SESSION['ret'] =  "Erreur lors de l'acces a la base de donnees.\n";
			return false;
		}
		$passwd = hash('whirlpool', $passwd);
		$array = array(
			$login,
			$passwd,
		);
		fputcsv($handle, $array, ",");
		fclose($handle);
		$_SESSION['ret'] = "Inscription OK !\n";
		return true;
	}
	else
	{
		$_SESSION['error'] =  "Erreur lors de l'inscription: login ou passwd incorrect.\n";
		return false;
	}
}
if ($_POST['login'] && $_POST['passwd'])
{
	if (stock_sign_in($_POST['login'], $_POST['passwd']))
	{
		session_start();
		$_SESSION['login'] = $_POST['login'];
		header("Location: /index.php");
		return(0);
	}
	else
	{
		header("Location: /view/awefqr.php");
		return(0);
	}
}

?>