<?php
function log_out()
{
	if (!$_SESSION['login'])
	{
		return false;
	}
	else
	{
		session_destroy();
	}
	return true;
}
session_start();
log_out();
header("Location: /view/connexion.php");
return (0);
?>