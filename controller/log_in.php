<?php
function log_in($login, $passwd)
{
	if ($login && $passwd)
	{
		$handle = fopen("accounts.csv", "r+");
		if (!$handle)
		{
			$_SESSION['ret'] = "Erreur lors de l'acces a la base de donnees";
			return false;
		}
		$passwd = hash('whirlpool', $passwd);
		while (($array = fgetcsv($handle, 0, ",")) !== false)
		{
			if ($array[0] === $login && $array[1] === $passwd)
			{
				$_SESSION['login'] = $login;
				return true;
			}
		}
		$_SESSION['error'] = "Erreur: login ou passwd incorrect.\n";
		return false;
	}
	else
	{
		$_SESSION['error'] = "Erreur: login ou passwd incorrect.\n";
		return false;
	}
}
session_start();
if ($_POST['login'] && $_POST['passwd'])
{
	
	if (log_in($_POST['login'], $_POST['passwd']))
	{
		header("Location: /index.php");
		return(0);
	}
	else
	{
		header("Location: /view/connexion.php");
		return(0);
	}
}
?>