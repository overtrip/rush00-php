<?php session_start();?>
<!DOCTYPE html>
<html>
	<head>
		<title>FruitsShelling</title>
		<link rel="stylesheet" type="text/css" href="/view/css/home.css">
		<link rel="stylesheet" type="text/css" href="/view/css/nav-bar.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>

	<body>
		<?php include "view/nav-bar.php" ?>
		<div class="container">
			<div class="top">
				<h1> Bienvenue chez nous.</h1>
				<?php if ($_SESSION['login']) {?>
				<form action="/controller/log_out.php" method="POST">
			   		<button type="submit" class="btn btn-sm button-log register-btn field-police">Deconexion</button>
			   	</form>
				<?php }?>
				<?php if (!$_SESSION['login']) {?>
				<a href="/view/connexion.php">
			   		<button type="button" class="btn btn-sm button-log register-btn field-police">Connexion</button>
			   	</a>
				<?php }?>
			</div>
			<div class="cart-box">
				<div class="cart-item">
					<img class="showreel-img" src="/img/banana.jpg"></img>
					<div class="showreel-description"> Banana </div>
				</div>
				<div class="cart-item">
					<img class="showreel-img" src="/img/banana.jpg"></img>
					<div class="showreel-description"> Banana </div>
				</div>
				<div class="cart-item">
					<img class="showreel-img" src="/img/banana.jpg"></img>
					<div class="showreel-description"> Banana </div>
			</div>
		  </div>
		</div>
	</body>
</html>
