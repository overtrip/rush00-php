<?php
	session_start();
	if ($_SESSION['ret'])
		header('index.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>FruitsShelling</title>
		<link rel="stylesheet" type="text/css" href="/view/css/home.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>

	<body>
		<div class="container">
			<div class="top">
				<h1> Bienvenue chez nous.</h1>
				<a href="/view/connexion.php">
				    <button type="button" class="btn btn-sm button-log register-btn field-police">Connection</button>
				</a>
				<form class="log-box" action="/controller/sign_in.php" method="POST">
				  <div class="form-group">
				  	<h1 class="title-box">Inscription</h1>
				    <label class="field-police">Utilisateur</label>
				    <input type="text" class="form-control" class="btn-log" placeholder="Utilisateur" name="login">
				    <label class="field-police">Mot de passe</label>
				    <input type="password" class="form-control" class="btn-log" placeholder="Mot de passe" name="passwd">
				    <button type="submit" class="btn btn-primary log-btn">J'arrive !</button>
				  </div>
				</form>
			</div>
		</div>
	</body>
</html>