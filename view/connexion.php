<?php
	session_start();
	if ($_SESSION['login'])
		header('Location: http://www.votresite.com/pageprotegee.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>FruitsShelling</title>
		<link rel="stylesheet" type="text/css" href="/view/css/home.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>

	<body>
		<div class="container">
			<div class="top">
				<h1> Bienvenue chez nous.</h1>
				<a href="/view/sign_in.php">
				    <button type="button" class="btn btn-sm button-log register-btn field-police">Inscription</button>
				</a>
				<form class="log-box" action="/controller/log_in.php" method="POST">
				  <div class="form-group">
				  	<h1 class="title-box">Connection</h1>
				    <label class="field-police">Utilisateur</label>
				    <input type="text" name="login" class="form-control btn-log" placeholder="Utilisateur">
				    <label class="field-police">Mot de passe</label>
				    <input type="password" name="passwd" class="form-control btn-log" placeholder="Mot de passe">
				    <button type="submit" class="btn btn-primary log-btn">J'arrive !</button>
				  </div>
				</form>
			</div>
		</div>
	</body>
</html>